#include "stdio.h"
#include "stdlib.h"
#define IN  
#define OUT  
#define min(a, b) ((a < b)? a : b)
#define max(a, b) ((a > b)? a : b)
class CConf
{
private:
	int n;
	int *mv;
	char **sv;
	int *ms;
	char **ss;
public:
	CConf(const char *);
	CConf(void);
	CConf(const CConf &);
	CConf & operator+(const CConf &);
	CConf & operator=(const CConf &);
	int Evalute(IN const char *, IN int m, OUT const char **);
	int Find(IN const char *);
}
